/**
 * 
 */
package it.unibo.oop.lab.enum1;

/**
 * Represents an enumeration for declaring sports;
 * 
 * 1) Complete the definition of the enumeration.
 * 
 */
//definisco l'enum, con gli elementi che ci voglio dentro 
public enum Sport {
	BASKET, SOCCER, TENNIS, BIKE, F1, MOTOGP, VOLLEY
}
